package org.miage.placesearcher.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lloison on 19/02/2018.
 */

public class Geometry extends Model{
    @Column(name = "label", index = true, unique = true)
    public String label;

    @Expose
    private List<Double> coordinates = new ArrayList<>();

    @Column(name="latitude")
    private Double latitude;

    @Column(name="longitude")
    private Double longitude;

    public Double getLatitude() {
        if (coordinates.size() >= 2) {
            return coordinates.get(1);
        }
        return 0d;
    }

    public Double getLongitude() {
        if (coordinates.size() >= 2) {
            return coordinates.get(0);
        }
        return 0d;
    }
}
